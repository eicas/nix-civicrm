# nix-civicrm

This repo holds the Nix definitions used to build the civicrm docker image
for EICAS.

## Building

`docker load <$(nix-build)`

## Installation

* Copy .env.example to .env, choosing your own passwords
* `docker-compose up`
* `source .env && echo "SET GLOBAL log_bin_trust_function_creators=1;" | docker exec -i $(docker ps | grep mysql | cut -d " " -f 1) mysql civicrm -u root -p$MYSQL_ROOT_PASSWORD`
