{ pkgs ? import <nixpkgs> {}
}:

with pkgs;

stdenv.mkDerivation rec {
  name = "civicrm";
  version = "5.49.3";
  src = fetchzip {
    url = "https://download.civicrm.org/civicrm-${version}-wordpress.zip";
    hash = "sha256-FWJZ8yr8EJlqLw42t0AH8yuqPDyRs4Da9Lu+Ie9rlTg=";
  };
  buildPhase = "";
  installPhase = ''
    mkdir -p $out
    cp -ra ${wordpress}/share/wordpress/* $out
    chmod a+wx $out/wp-content/plugins
    mkdir $out/wp-content/plugins/civicrm
    cp -ra * $out/wp-content/plugins/civicrm
    cp ${./wp-config.php} $out/wp-config.php
  '';
}
