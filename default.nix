{ pkgs ? import <nixpkgs> {}
, port ? "8080"
}:

with pkgs;

let
  civicrm = callPackage ./civicrm.nix {};
  # should match docker-compose.yml
  root = "webroot";
  nginx = callPackage ./nginx.nix {
    inherit port;
    root = "/${root}";
  };
in
dockerTools.buildLayeredImage {
  name = "raboof/civicrm";
  tag = "latest";
  contents = [
    # needed for nginx
    dockerTools.fakeNss
    # only enable for debugging
    bash vim coreutils findutils procps
  ];
  extraCommands = ''
    # TODO put this in a different 'output'
    ${nginx}/bin/extraCommands

    mkdir ${root}
    # would be nicer to symlink, but somehow something determines
    # the path to the upload dir relative to itself...
    cp -ra ${civicrm}/* ${root}
  '';
  config = {
    Cmd = [ "${nginx}/bin/start" ];
    ExposedPorts = {
      "${port}/tcp" = {};
    };
  };
}
