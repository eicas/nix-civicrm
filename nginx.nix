{ pkgs ? import <nixpkgs> {}
, port
, root
}:

with pkgs;

let
  phpFpmSocketLocation = "/run/php-fpm.sock";
  nginxConf = writeText "nginx.conf" ''
    user root nobody;
    daemon off;
    error_log /dev/stdout info;
    pid /dev/null;
    events {}
    http {
      access_log /dev/stdout;
      include ${nginx}/conf/mime.types;
      server {
        listen ${port};
        index index.php;
        root ${root};
        client_max_body_size 16M;
        # https://civicrm.org/advisory/civi-sa-2014-001-risk-information-disclosure
        location ~ wp-content/plugins/files/civicrm/ConfigAndLog.* {
          deny all;
        }
        location ~ wp-content/uploads/civicrm/ConfigAndLog.* {
          deny all;
        }
        location ~ wp-content/plugins/files/civicrm/templates_c.* {
          deny all;
        }
        location ~ wp-content/uploads/civicrm/templates_c.* {
          deny all;
        }
        location ~ wp-content/plugins/files/civicrm/upload.* {
          deny all;
        }
        location ~ wp-content/uploads/civicrm/upload.* {
          deny all;
        }
        location ~ wp-content/plugins/files/civicrm/custom.* {
          deny all;
        }
        location ~ wp-content/uploads/civicrm/custom.* {
          deny all;
        }
        location ~ [^/]\.php(/|$) {
          root ${root};
          fastcgi_split_path_info ^(.+?\.php)(/.*)$;
          if (!-f $document_root$fastcgi_script_name) {
            return 404;
          }
          # Mitigate https://httpoxy.org/ vulnerabilities
          fastcgi_param HTTP_PROXY "";
          fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
          fastcgi_pass unix:${phpFpmSocketLocation};
          fastcgi_index index.php;
          include ${nginx}/conf/fastcgi_params;
        }
      }
    }
  '';
   phpFpmCfg = writeText "php-fpm.conf" ''
    [global]
    daemonize=yes
    error_log=/proc/self/fd/2

    [www]
    user = nobody
    group = nobody
    listen = ${phpFpmSocketLocation}
    pm = static
    pm.max_children = 5

    ; to read e.g. the database config passed in via the env.
    ; there is nothing (more) sensitive in there anyway:
    clear_env=no

    access.log=/proc/self/fd/2
    catch_workers_output = yes
    php_flag[display_errors] = on
    php_admin_value[error_log] = /proc/self/fd/2
    php_admin_flag[log_errors] = on
  '';
  phpIni = writeText "php.ini" ''
    post_max_size = 32M
    upload_max_filesize = 32M
    openssl.cafile=${cacert}/etc/ssl/certs/ca-bundle.crt
  '';
   extraCommands = writeScript "extraCommands.sh" ''
    mkdir -p var/log/nginx
    mkdir -p var/cache/nginx
    mkdir -p run
    touch run/php-fpm.sock

    # for the php-fpm lock file:
    mkdir -p tmp
    chmod a+rwx tmp
  '';
  startScript = writeScript "start" ''
    #!${bash}/bin/sh
    ${php74}/bin/php-fpm -y ${phpFpmCfg} -c ${phpIni}
    exec "${nginx}/bin/nginx" "-c" ${nginxConf}
  '';
in
stdenv.mkDerivation {
  name = "nginx";
  version = "tag";
  unpackPhase = "true";

  installPhase = ''
    runHook preInstall

    mkdir -p $out/bin
    cp ${extraCommands} $out/bin/extraCommands
    cp ${startScript} $out/bin/start

    runHook postInstall
  '';
}
